using System;

namespace Refactoring
{
    public class Item
    {
        public int Days { set; get; }
        public int Seats { set; get; }
        public int Available { set; get; }
        public Boolean Online { set; get; }
        public String Type { set; get; }
        public int Price { set; get; }
        public int Full { set; get; }

        public Item(int d, int s, int a, Boolean online, String type, int full)
        {
            this.Days = d;
            this.Seats = s;
            this.Available = a;
            this.Online = online;
            this.Type = type;
            this.Price = full;
            this.Full = full;
        }
    }
}