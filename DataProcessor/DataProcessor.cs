﻿using System;
using System.Collections.Generic;

namespace Refactoring
{
    public class DataProcessor
    {
               
        public static int Value;

        public static List<Item> ProcessData(List<Item> itemList)
        {
            Value = 0;

		for (int i = 0; i < itemList.Count; i++) {

			itemList[i].Days -= 1;

			if (itemList[i].Days <= 10) {

				if (itemList[i].Days <= 1 || (itemList[i].Available < 3 && itemList[i].Days <= 5)) {
					itemList[i].Price = itemList[i].Full;
				} else {
					if (itemList[i].Type.Equals("CSD")) {
						itemList[i].Price = itemList[i].Full - (itemList[i].Days * 30);
					} else {
						itemList[i].Price = itemList[i].Full - (itemList[i].Days * 20);
					}
				}

			} else if (itemList[i].Days > 10) {

				if (itemList[i].Days <= 1 || (itemList[i].Available < 3 && itemList[i].Days <= 5)) {
					itemList[i].Price = itemList[i].Full;
				} else {
					if (itemList[i].Type.Equals("CSM")) {
						itemList[i].Price = itemList[i].Full - 500;
					} else {
						itemList[i].Price = itemList[i].Full - 400;
					}
				}
			}

			if (itemList[i].Type.Equals("CSD") && itemList[i].Price < 900) {
				itemList[i].Price = 900;
			} else if (itemList[i].Type.Equals("CSM") && itemList[i].Price < 1000) {
				itemList[i].Price = 1000;
			} else if (itemList[i].Type.Equals("CSPO") && itemList[i].Price < 1200) {
				itemList[i].Price = 1200;
			}

			Value += (itemList[i].Available * itemList[i].Price);

		}

		return itemList;
        }
    }
}